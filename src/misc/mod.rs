//! Misc common code used in many places.
pub mod once;

pub use once::Once;
